# Hardware Specifications



### ALIASES
- true = 1
- false = 0



### FLAGS

By default, all flags are set to 0.

- cmp = Flag used by the `CMP` instruction.
- neg = Set to `true` if the previous instruction resulted in a negative value.
It could also be set to `true` if the previous `CMP` instruction resulted in a lesser than.
- gt = Set to `true` if the previous `CMP` instruction resulted in a greater than.



### REGISTERS

All general purpose registers hold 64 bits (8 bytes).

There are 16 general purpose registers.

There is an instruction pointer register.
Which stores the current instructions beginning memory address.
(One instruction spans more than one memory address.)

Every multi-byte value in RAM is stored in little-endian format. (Small end first.)

#### Special Registers:

> NOTE: The number prefix is what the special register's ID in bytecode is,
so for example, all the general purpose registers are marked
with anything from 0 as the minumum, and 15 as the maximum.
So an instruction like `LOAD r7, $ip` would become (in hex): `<OPCODE> 07 10`.
As you can see, `07` is general purpose register 7, and `10` (16 in hex) is
the instruction pointer register.

16. ip = Instruction Pointer *`(read-only)`*
17. er = Execution Ring (User mode is 1, kernel mode is 0.) *`(read-only)`*
18. sp = Stack Pointer *`(read/write)`*
19. bp = Base Pointer *`(read/write)`*



### MEMORY

A memory address is 64 bits in length (the minumum address is 0, and the maximum address is 18446744073709551615).

Every location at a memory address can store 8 bits (1 byte) of data.
