# ISA - (I)nstruction (S)et (A)rchitecture

> NOTE: This file was auto-generated via the files in the `isa_src` directory.



### ALIASES

- true = 1
- false = 0



### INFORMATION

- Instructions are variable width. This means that every instruction is
a different size of bytes in memory.
- In the ISA docs, the size (in bytes) field shows the size including the opcode. (So the full instruction size, not just the arguments size).
- The size of an instruction is found via the opcode. (No special rules, just pattern matching essentially.)
- The endian is little-endian.
- Any multi-byte value is stored in memory as little-endian. (Registers, etc...)
- Instructions are actually stored in big-endian format in RAM. This is to make them easier to
work with since they are all of different sizes.
- When an instruction is stored in memory, the opcode is stored before the argument bytes.



### CPU BINARY INSTRUCTIONS

- Instruction Count: ${INS_COUNT}
- Unique OpCode Count: ${OPCODE_COUNT}
- Biggest Instruction's Size (In Bytes): ${MAX_INS_SIZE}
${INS_TABLES}

### Clarifications

- Calling a subroutine is when you push the instruction pointer register's
value offset by the size of the `CALL` instruction variant (current instruction)
onto the stack, and then you push the base pointer register's value
onto the stack, and then you jump to the subroutine's address.

- Returning from a subroutine is when you copy the value of the base pointer
into the stack pointer, and then you pop the stack into the base pointer,
and then you pop the stack and jump to the popped value as an address.

- When talking about multi-byte values, little half refers to the side with
the least significant bit, and big half refers to the side
with the most significant bit.
#### Example of little half vs big half:
```
16-Bit Value (In Hex): c0fe

Big Half: c0
Little Half: fe
```

- When multi-byte values are stored in RAM, they are stored in more than one address.
#### Examples of how bigger-than-1-byte values are stored in RAM:
```
<< STORING A REGISTER IN RAM >>

STR r0, 0x0000FDEA   (0x0000FDEA is hexadecimal for the decimal number 65002.)

Inside Register 0: 0x0000C0FE   (0x0000C0FE is hexadecimal for the decimal number 49406.)

(
    FIY: '0x' is a prefix that means the next characters are in hex.
    Example: 0xFEA0 means hexadecimal number FEA0.
)

---- Memory After Operation ----
0x0000FDEA (65002): 0xFE   (Small half of the register.)
0x0000FDEB (65003): 0xC0
0x0000FDEB (65003): 0x00
0x0000FDEB (65003): 0x00   (Big half of the register.)

Putting this info back into a register via something like a LOAD instruction
should be apparent on how to do, considering it is basically just replaying this
backwards.
```

- When you read the descriptions of the `LOAD` and `STR` instructions, you may
be thrown off by the fact that the little half of a register is stored first in memory.
This is actually on purpose. It is because this CPU stores bytes in little-endian format.
