#!/usr/bin/env bash

export MAX_INS_SIZE=0
export INS_COUNT=0
export OPCODE_COUNT=0

tmp_ins_txt_path="/tmp/ISA_BUILD_ins_txt.txt"

log_error () {
    echo -e "\033[0;31m[\033[0m \033[1;31mERROR\033[0m \033[0;31m]:\033[0m $1\033[0m" >&2
}

log_info () {
    echo -e "\033[0;32m[\033[0m \033[1;32mINFO\033[0m \033[0;32m]:\033[0m $1\033[0m"
}

log_debug () {
    if [[ "${DEBUG_LOG}" == "1" ]]; then
        echo -e "\033[0;35m[\033[0m \033[1;35mDEBUG\033[0m \033[0;35m]:\033[0m $1\033[0m"
    fi
}

die () {
    log_error "$1"
    exit 1
}

rep_string () {
    final_string=""

    for (( i=0; i<${2}; i++ )); do
        final_string+="${1}"
    done

    echo "${final_string}"
}

clamp_zero () {
    if [[ "$1" -lt 0 ]]; then
        echo "0"
    else
        echo "$1"
    fi
}

# Get size of type in bytes.
size_of () {
    case $1 in
    "reg")
        echo "1"
    ;;
    "int")
        echo "8"
    ;;
    "mem")
        echo "8"
    ;;
    *)
        die "Found invalid type: '$1'"
    ;;
    esac
}

[[ -f template.md ]] || die "Please run in the parent directory of this script!"

log_debug "Debug log is on! (DEBUG_LOG == 1)"

ins_files_count=$(find ./instructions -type f | wc -l)
ins_order_count=$(cat ./ins_order.txt | wc -l)

if [[ "${ins_files_count}" == "${ins_order_count}" ]]; then
    log_info "Successfully verified that all instructions are accounted for in: './ins_order.txt'"
else
    die "File count is not the same as order count! ('./ins_order.txt')\nFile Count: ${ins_files_count}\nOrder Count: ${ins_order_count}"
fi

ins_txt=()

while IFS= read -r i; do
    i="./instructions/${i}.txt"

    [[ -f "${i}" ]] || die "No such file: '${i}'"

    c_name="$(basename "${i}" | sed 's/.txt//g' | tr '_' ' ')"

    log_info "Building Category: '\033[1;36m${c_name}\033[0m'"

    md_table=""
    previous_ins=""
    to_parse=""
    while IFS= read -r line; do
        if [[ "${line}" == "" ]]; then
            continue;
        fi

        if [[ "${line}" == "|=> "* ]]; then
            to_parse="${line}"

            continue;
        elif [[ "${line}" == ";;" ]]; then
            :
        else
            to_parse="${to_parse} ${line}"

            continue;
        fi

        p_ins="$(echo "${to_parse}" | awk -F ' := ' '{ print $1 }' | sed 's/|=> //g')"
        p_opcode="$(echo "${to_parse}" | awk -F ' := ' '{print $2}')"
        p_args_txt="$(echo "${to_parse}" | awk -F ' := ' '{print $3}')"
        p_desc="$(echo "${to_parse}" | awk -F ' := ' '{print $4}')"

        len_opcode="$(($(echo "${p_opcode}" | wc -m) - 1))"
        len_args_txt="$(($(echo "${p_args_txt}" | wc -m) - 1))"

        instruction_size=$(( $(( ${len_opcode} - 2 )) / 2 ))
        for arr_i in $(echo "${p_args_txt}" | sed 's/\[//' | sed 's/]//g' | sed 's/, /:/g' | tr ':' '\n'); do
            instruction_size=$(( instruction_size + $(size_of "${arr_i}") ))
        done

        if [[ ${MAX_INS_SIZE} -lt ${instruction_size} ]]; then
            log_debug "Setting \$MAX_INS_SIZE to: ${instruction_size}"
            export MAX_INS_SIZE=${instruction_size}
        fi

        mdh_opcode=" OpCode "
        mdh_args=" Arguments "
        mdh_size=" Instruction Size (In Bytes) "
        mdh_desc=" Description "

        mdh_opcode_len="$(($(echo "${mdh_opcode}" | wc -m) - 1))"
        mdh_args_len="$(($(echo "${mdh_args}" | wc -m) - 1))"
        mdh_size_len="$(($(echo "${mdh_size}" | wc -m) - 1))"
        mdh_desc_len="$(($(echo "${mdh_desc}" | wc -m) - 1))"

        mdh_opcode_diff=$(clamp_zero $((${len_opcode} - ${mdh_opcode_len})))
        mdh_args_diff=$(clamp_zero $((${len_args_txt} - ${mdh_args_len})))

        if [[ "${p_ins}" == "${previous_ins}" ]]; then
            log_debug "Appending to already encountered instruction..."
        else
            log_debug "Incrementing instruction count..."
            export INS_COUNT=$(( ${INS_COUNT} + 1 ))

            ins_txt+="${md_table}\n"

            md_table="### *\`${p_ins}\`*:\n"

            md_table+="|${mdh_opcode}|${mdh_args}"
            md_table+=$(rep_string " " ${mdh_args_diff})
            md_table+="|${mdh_size}| Description |\n"

            md_table+="|"
            md_table+=$(rep_string "-" $(( ${mdh_opcode_len} + ${mdh_opcode_diff} )))
            md_table+="|"
            md_table+=$(rep_string "-" $(( ${mdh_args_len} + ${mdh_args_diff} )))
            md_table+="|"
            md_table+=$(rep_string "-" ${mdh_size_len})
            md_table+="|"
            md_table+=$(rep_string "-" ${mdh_desc_len})
            md_table+="|"
            md_table+="\n"
        fi

        log_debug "Incrementing opcode count..."
        export OPCODE_COUNT=$(( ${OPCODE_COUNT} + 1 ))

        md_table+="| ${p_opcode} "
        md_table+="| ${p_args_txt} "
        md_table+="| ${instruction_size} "
        md_table+="| ${p_desc} |\n"

        previous_ins="${p_ins}"
    done < "${i}"

    [[ "${md_table}" == "" ]] || ins_txt+="${md_table}\n"
done < "./ins_order.txt"

log_info "Reading template..."
template_txt="$(cat ./template.md || die "Failed to read template Markdown file!")"

log_info "Mixing in data..."
new_txt="${template_txt}"
mix_data () {
    log_debug "Mixing in data into: \${$1}"
    new_txt="$(echo "${new_txt}" | sed "s/\${$1\}/$2/g" || die "Failed to mix in data!")"
}
mix_data "INS_TABLES" "${ins_txt}"
mix_data "MAX_INS_SIZE" "${MAX_INS_SIZE}"
mix_data "INS_COUNT" "${INS_COUNT}"
mix_data "OPCODE_COUNT" "${OPCODE_COUNT}"

log_info "Writing new ISA Markdown to: ../ISA.md"
echo "${new_txt}" > ../ISA.md || die "Failed to write new ISA Markdown!"

log_info "Successfully built new ../ISA.md file!"
