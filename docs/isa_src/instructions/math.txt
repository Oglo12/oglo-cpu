|=> ADD := 0x0200 := [reg, reg, reg] := Add up the values in `1st param` and
`2nd param` registers, then store the result in `3rd param` register.
;;

|=> SUB := 0x0210 := [reg, reg, reg] := Subtract `2nd param` register's value
from `1st param` register's value, then store the result in `3rd param`
register.
;;

|=> MUL := 0x0220 := [reg, reg, reg] := Multiply the values in `1st param` and
`2nd param` registers, then store the result in `3rd param` register.
;;

|=> DIV := 0x0230 := [reg, reg, reg] := Divide `1st param` register's value
by `2nd param` register's value, then store the result in `3rd param`
register.
;;
