|=> LOAD := 0x0100 := [reg, mem] := Load a sequence of bytes from memory starting
at the specified address into the specified register.
;;

|=> LOAD := 0x0101 := [reg, int] := Doesn't use RAM at all, just put an integer
into a register. Simple as that.
;;

|=> LOAD := 0x0102 := [reg, reg] := Load a sequence of bytes from memory starting
at the memory address specified inside `2nd param` register into `1st param`
register.
;;

|=> STR := 0x0110 := [reg, mem] := Store a sequence of bytes from the specified
register into memory starting at the specified address.
;;

|=> STR := 0x0111 := [reg, reg] := Store a sequence of bytes from the `1st param`
register into memory starting at the address inside of `2nd param` register.
;;

|=> COPY := 0x0120 := [reg, reg] := Copy the value from `1st param` register into
`2nd param` register.
;;

|=> CTD := 0x0130 := [reg, reg] := Copy all the data from a memory location
until a NULL byte (0) is hit. The source address is specified in `1st param`
register, and the destination address is specified in `2nd param` register.
This instruction can be used to do things such as copy a NULL terminated string
to another memory location. (The NULL byte is also copied.)
;;

|=> CMEM := 0x0140 := [reg, reg, int] := Copy all the data from a memory location
until the number of copied bytes is equal to the specified integer. The source
address is specified in `1st param` register, and the destination address is
specified in `2nd param` register. This can be used for copying integers and
stuff around in memory since they are more than one byte in length.
For example, a 64-bit integer is 8 bytes long, so you could copy an integer
with something like this: `CMEM r0, r1, 8`
;;

|=> TLEN := 0x0150 := [reg, mem] := Count how many bytes there are in a sequence
of memory locations until a NULL byte is hit. An example usage of this
instruction would be to get the length of a NULL terminated string.
(The NULL byte gets excluded in the final length.)
;;
