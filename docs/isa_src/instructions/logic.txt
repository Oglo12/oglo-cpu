|=> CMP := 0x0700 := [reg, reg] := Compare specified registers' values as
`1st param` left-hand-side (lhs), and `2nd param` right-hand-side (rhs).
Set `cmp` flag to the result of subtracting rhs from lhs in unsigned integer
math (under 0 values wrap around).
Set `neg` flag to true if the resulting math used to set `cmp` flag would've
resulted in a negative number in signed integer math.
Set `gt` flag to true if lhs is more than rhs.
(Quick Note: Any flags changed by subtracting values in registers via the
`SUB` instruction are also changed when setting `cmp` flag.)
;;

|=> CMP := 0x0701 := [reg, int] := Same as CMP with two registers, but with a
register and an integer instead.
;;

|=> CMP := 0x0702 := [int, reg] := Same as CMP with two registers, but with a
register and an integer instead.
;;
