|=> HLT := 0x0000 := [] := Stop the CPU. (End of execution.)
;;

|=> EUM := 0x0010 := [] := Enter user mode. (Userland programs.)
;;

|=> SYS := 0x0020 := [] := Make a syscall.
;;
