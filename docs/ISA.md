# ISA - (I)nstruction (S)et (A)rchitecture

> NOTE: This file was auto-generated via the files in the `isa_src` directory.



### ALIASES

- true = 1
- false = 0



### INFORMATION

- Instructions are variable width. This means that every instruction is
a different size of bytes in memory.
- In the ISA docs, the size (in bytes) field shows the size including the opcode. (So the full instruction size, not just the arguments size).
- The size of an instruction is found via the opcode. (No special rules, just pattern matching essentially.)
- The endian is little-endian.
- Any multi-byte value is stored in memory as little-endian. (Registers, etc...)
- Instructions are actually stored in big-endian format in RAM. This is to make them easier to
work with since they are all of different sizes.
- When an instruction is stored in memory, the opcode is stored before the argument bytes.



### CPU BINARY INSTRUCTIONS

- Instruction Count: 39
- Unique OpCode Count: 58
- Biggest Instruction's Size (In Bytes): 12

### *`HLT`*:
| OpCode | Arguments | Instruction Size (In Bytes) | Description |
|--------|-----------|-----------------------------|-------------|
| 0x0000 | [] | 2 | Stop the CPU. (End of execution.) |

### *`EUM`*:
| OpCode | Arguments | Instruction Size (In Bytes) | Description |
|--------|-----------|-----------------------------|-------------|
| 0x0010 | [] | 2 | Enter user mode. (Userland programs.) |

### *`SYS`*:
| OpCode | Arguments | Instruction Size (In Bytes) | Description |
|--------|-----------|-----------------------------|-------------|
| 0x0020 | [] | 2 | Make a syscall. |


### *`LOAD`*:
| OpCode | Arguments | Instruction Size (In Bytes) | Description |
|--------|-----------|-----------------------------|-------------|
| 0x0100 | [reg, mem] | 11 | Load a sequence of bytes from memory starting at the specified address into the specified register. |
| 0x0101 | [reg, int] | 11 | Doesn't use RAM at all, just put an integer into a register. Simple as that. |
| 0x0102 | [reg, reg] | 4 | Load a sequence of bytes from memory starting at the memory address specified inside `2nd param` register into `1st param` register. |

### *`STR`*:
| OpCode | Arguments | Instruction Size (In Bytes) | Description |
|--------|-----------|-----------------------------|-------------|
| 0x0110 | [reg, mem] | 11 | Store a sequence of bytes from the specified register into memory starting at the specified address. |
| 0x0111 | [reg, reg] | 4 | Store a sequence of bytes from the `1st param` register into memory starting at the address inside of `2nd param` register. |

### *`COPY`*:
| OpCode | Arguments | Instruction Size (In Bytes) | Description |
|--------|-----------|-----------------------------|-------------|
| 0x0120 | [reg, reg] | 4 | Copy the value from `1st param` register into `2nd param` register. |

### *`CTD`*:
| OpCode | Arguments | Instruction Size (In Bytes) | Description |
|--------|-----------|-----------------------------|-------------|
| 0x0130 | [reg, reg] | 4 | Copy all the data from a memory location until a NULL byte (0) is hit. The source address is specified in `1st param` register, and the destination address is specified in `2nd param` register. This instruction can be used to do things such as copy a NULL terminated string to another memory location. (The NULL byte is also copied.) |

### *`CMEM`*:
| OpCode | Arguments     | Instruction Size (In Bytes) | Description |
|--------|---------------|-----------------------------|-------------|
| 0x0140 | [reg, reg, int] | 12 | Copy all the data from a memory location until the number of copied bytes is equal to the specified integer. The source address is specified in `1st param` register, and the destination address is specified in `2nd param` register. This can be used for copying integers and stuff around in memory since they are more than one byte in length. For example, a 64-bit integer is 8 bytes long, so you could copy an integer with something like this: `CMEM r0, r1, 8` |

### *`TLEN`*:
| OpCode | Arguments | Instruction Size (In Bytes) | Description |
|--------|-----------|-----------------------------|-------------|
| 0x0150 | [reg, mem] | 11 | Count how many bytes there are in a sequence of memory locations until a NULL byte is hit. An example usage of this instruction would be to get the length of a NULL terminated string. (The NULL byte gets excluded in the final length.) |


### *`ADD`*:
| OpCode | Arguments     | Instruction Size (In Bytes) | Description |
|--------|---------------|-----------------------------|-------------|
| 0x0200 | [reg, reg, reg] | 5 | Add up the values in `1st param` and `2nd param` registers, then store the result in `3rd param` register. |

### *`SUB`*:
| OpCode | Arguments     | Instruction Size (In Bytes) | Description |
|--------|---------------|-----------------------------|-------------|
| 0x0210 | [reg, reg, reg] | 5 | Subtract `2nd param` register's value from `1st param` register's value, then store the result in `3rd param` register. |

### *`MUL`*:
| OpCode | Arguments     | Instruction Size (In Bytes) | Description |
|--------|---------------|-----------------------------|-------------|
| 0x0220 | [reg, reg, reg] | 5 | Multiply the values in `1st param` and `2nd param` registers, then store the result in `3rd param` register. |

### *`DIV`*:
| OpCode | Arguments     | Instruction Size (In Bytes) | Description |
|--------|---------------|-----------------------------|-------------|
| 0x0230 | [reg, reg, reg] | 5 | Divide `1st param` register's value by `2nd param` register's value, then store the result in `3rd param` register. |


### *`BSL`*:
| OpCode | Arguments     | Instruction Size (In Bytes) | Description |
|--------|---------------|-----------------------------|-------------|
| 0x0300 | [reg, reg, reg] | 5 | Bit-shift `1st param` register's value by `2nd param` register's value to the left, then store the result in `3rd param` register. |

### *`BSR`*:
| OpCode | Arguments     | Instruction Size (In Bytes) | Description |
|--------|---------------|-----------------------------|-------------|
| 0x0310 | [reg, reg, reg] | 5 | Bit-shift `1st param` register's value by `2nd param` register's value to the right, then store the result in `3rd param` register. |

### *`BRL`*:
| OpCode | Arguments     | Instruction Size (In Bytes) | Description |
|--------|---------------|-----------------------------|-------------|
| 0x0320 | [reg, reg, reg] | 5 | Bit-rotate `1st param` register's value by `2nd param` register's value to the left, then store the result in `3rd param` register. |

### *`BRR`*:
| OpCode | Arguments     | Instruction Size (In Bytes) | Description |
|--------|---------------|-----------------------------|-------------|
| 0x0330 | [reg, reg, reg] | 5 | Bit-rotate `1st param` register's value by `2nd param` register's value to the right, then store the result in `3rd param` register. |


### *`BAND`*:
| OpCode | Arguments     | Instruction Size (In Bytes) | Description |
|--------|---------------|-----------------------------|-------------|
| 0x0400 | [reg, reg, reg] | 5 | Bit-and `1st param` and `2nd param` registers' values, then store the result in `3rd param` register. |

### *`BOR`*:
| OpCode | Arguments     | Instruction Size (In Bytes) | Description |
|--------|---------------|-----------------------------|-------------|
| 0x0410 | [reg, reg, reg] | 5 | Bit-or `1st param` and `2nd param` registers' values, then store the result in `3rd param` register. |

### *`BXOR`*:
| OpCode | Arguments     | Instruction Size (In Bytes) | Description |
|--------|---------------|-----------------------------|-------------|
| 0x0420 | [reg, reg, reg] | 5 | Bit-xor `1st param` and `2nd param` registers' values, then store the result in `3rd param` register. |

### *`BNOT`*:
| OpCode | Arguments | Instruction Size (In Bytes) | Description |
|--------|-----------|-----------------------------|-------------|
| 0x0430 | [reg, reg] | 4 | Bit-not (invert bits) `1st param` register's value, then store the result in `2nd param` register. |


### *`JUMP`*:
| OpCode | Arguments | Instruction Size (In Bytes) | Description |
|--------|-----------|-----------------------------|-------------|
| 0x0500 | [mem] | 10 | Jump to the specified memory address. (Jumping is moving the instruction pointer, then resuming execution at that new location.) |
| 0x0501 | [reg] | 3 | Jump to the memory address inside of the specified register. |

### *`JIZ`*:
| OpCode | Arguments | Instruction Size (In Bytes) | Description |
|--------|-----------|-----------------------------|-------------|
| 0x0510 | [mem] | 10 | Jump to the specified memory address if the `cmp` flag is set to 0. |
| 0x0511 | [reg] | 3 | Jump to the memory address inside of the specified register if the `cmp` flag is set to 0. |

### *`JNZ`*:
| OpCode | Arguments | Instruction Size (In Bytes) | Description |
|--------|-----------|-----------------------------|-------------|
| 0x0520 | [mem] | 10 | Jump to the specified memory address if the `cmp` flag is not set to 0. |
| 0x0521 | [reg] | 3 | Jump to the memory address inside of the specified register if the `cmp` flag is not set to 0. |

### *`JIL`*:
| OpCode | Arguments | Instruction Size (In Bytes) | Description |
|--------|-----------|-----------------------------|-------------|
| 0x0530 | [mem] | 10 | Jump to the specified memory address if the `neg` flag is set to true. |
| 0x0531 | [reg] | 3 | Jump to the memory address inside of the specified register if the `neg` flag is set to true. |

### *`JNL`*:
| OpCode | Arguments | Instruction Size (In Bytes) | Description |
|--------|-----------|-----------------------------|-------------|
| 0x0540 | [mem] | 10 | Jump to the specified memory address if the `neg` flag is set to false. |
| 0x0541 | [reg] | 3 | Jump to the memory address inside of the specified register if the `neg` flag is set to false. |

### *`JIG`*:
| OpCode | Arguments | Instruction Size (In Bytes) | Description |
|--------|-----------|-----------------------------|-------------|
| 0x0550 | [mem] | 10 | Jump to the specified memory address if the `gt` flag is set to true. |
| 0x0551 | [reg] | 3 | Jump to the memory address inside of the specified register if the `gt` flag is set to true. |

### *`JNG`*:
| OpCode | Arguments | Instruction Size (In Bytes) | Description |
|--------|-----------|-----------------------------|-------------|
| 0x0560 | [mem] | 10 | Jump to the specified memory address if the `gt` flag is set to false. |
| 0x0561 | [reg] | 3 | Jump to the memory address inside of the specified register if the `gt` flag is set to false. |


### *`CALL`*:
| OpCode | Arguments | Instruction Size (In Bytes) | Description |
|--------|-----------|-----------------------------|-------------|
| 0x0600 | [mem] | 10 | Call the specified memory address (jump to subroutine). |
| 0x0601 | [reg] | 3 | Call the memory address inside of the specified register (jump to subroutine). |

### *`RET`*:
| OpCode | Arguments | Instruction Size (In Bytes) | Description |
|--------|-----------|-----------------------------|-------------|
| 0x0610 | [] | 2 | Return from subroutine. |

### *`PUSH`*:
| OpCode | Arguments | Instruction Size (In Bytes) | Description |
|--------|-----------|-----------------------------|-------------|
| 0x0620 | [reg] | 3 | Push the specified register's value onto the stack. |

### *`POP`*:
| OpCode | Arguments | Instruction Size (In Bytes) | Description |
|--------|-----------|-----------------------------|-------------|
| 0x0630 | [reg] | 3 | Pop off the stack and store the value into the specified register. |


### *`CMP`*:
| OpCode | Arguments | Instruction Size (In Bytes) | Description |
|--------|-----------|-----------------------------|-------------|
| 0x0700 | [reg, reg] | 4 | Compare specified registers' values as `1st param` left-hand-side (lhs), and `2nd param` right-hand-side (rhs). Set `cmp` flag to the result of subtracting rhs from lhs in unsigned integer math (under 0 values wrap around). Set `neg` flag to true if the resulting math used to set `cmp` flag would've resulted in a negative number in signed integer math. Set `gt` flag to true if lhs is more than rhs. (Quick Note: Any flags changed by subtracting values in registers via the `SUB` instruction are also changed when setting `cmp` flag.) |
| 0x0701 | [reg, int] | 11 | Same as CMP with two registers, but with a register and an integer instead. |
| 0x0702 | [int, reg] | 11 | Same as CMP with two registers, but with a register and an integer instead. |


### *`NOP`*:
| OpCode | Arguments | Instruction Size (In Bytes) | Description |
|--------|-----------|-----------------------------|-------------|
| 0x0800 | [] | 2 | Do nothing for 1 clock cycle. |
| 0x0801 | [int] | 10 | Do nothing for the specified amount of clock cycles. |
| 0x0802 | [reg] | 3 | Do nothing for the specified amount of clock cycles. (Specified amount of clock cycles is inside the register.) |


### *`ASSERT`*:
| OpCode | Arguments | Instruction Size (In Bytes) | Description |
|--------|-----------|-----------------------------|-------------|
| 0x0900 | [reg, int] | 11 | Assert that the specified register's value matches the specified integer. On hardware, this instruction does nothing by default, but kernel-level code can configure it to jump to some other instructions. This instruction is useful for things like emulators, which can do whatever they want with this instruction as long as the kernel-level code hasn't claimed it for something else. |
| 0x0901 | [reg, reg] | 4 | Same as ASSERT with [reg, int] but the integer is the `2nd param` register's value. |

### *`ACFG`*:
| OpCode | Arguments | Instruction Size (In Bytes) | Description |
|--------|-----------|-----------------------------|-------------|
| 0x0910 | [mem] | 10 | Configure the ASSERT instruction to jump to the specified address when an assert fails. |
| 0x0911 | [reg] | 3 | Same as ACFG with [mem] but the memory address is the specified register's value. |

### *`UNACFG`*:
| OpCode | Arguments | Instruction Size (In Bytes) | Description |
|--------|-----------|-----------------------------|-------------|
| 0x0920 | [] | 2 | Unconfigure the ASSERT instruction. (Don't jump anywhere when hit. Default behavior.) |


### *`INC`*:
| OpCode | Arguments | Instruction Size (In Bytes) | Description |
|--------|-----------|-----------------------------|-------------|
| 0x0A00 | [reg] | 3 | Increment specified register's stored value by 1. |
| 0x0A01 | [reg, int] | 11 | Same as `INC` with only a register, but also allows you to specify what to increment by. |

### *`DEC`*:
| OpCode | Arguments | Instruction Size (In Bytes) | Description |
|--------|-----------|-----------------------------|-------------|
| 0x0A10 | [reg] | 3 | Decrement specified register's stored value by 1. |
| 0x0A11 | [reg, int] | 11 | Same as `DEC` with only a register, but also allows you to specify what to decrement by. |



### Clarifications

- Calling a subroutine is when you push the instruction pointer register's
value offset by the size of the `CALL` instruction variant (current instruction)
onto the stack, and then you push the base pointer register's value
onto the stack, and then you jump to the subroutine's address.

- Returning from a subroutine is when you copy the value of the base pointer
into the stack pointer, and then you pop the stack into the base pointer,
and then you pop the stack and jump to the popped value as an address.

- When talking about multi-byte values, little half refers to the side with
the least significant bit, and big half refers to the side
with the most significant bit.
#### Example of little half vs big half:
```
16-Bit Value (In Hex): c0fe

Big Half: c0
Little Half: fe
```

- When multi-byte values are stored in RAM, they are stored in more than one address.
#### Examples of how bigger-than-1-byte values are stored in RAM:
```
<< STORING A REGISTER IN RAM >>

STR r0, 0x0000FDEA   (0x0000FDEA is hexadecimal for the decimal number 65002.)

Inside Register 0: 0x0000C0FE   (0x0000C0FE is hexadecimal for the decimal number 49406.)

(
    FIY: '0x' is a prefix that means the next characters are in hex.
    Example: 0xFEA0 means hexadecimal number FEA0.
)

---- Memory After Operation ----
0x0000FDEA (65002): 0xFE   (Small half of the register.)
0x0000FDEB (65003): 0xC0
0x0000FDEB (65003): 0x00
0x0000FDEB (65003): 0x00   (Big half of the register.)

Putting this info back into a register via something like a LOAD instruction
should be apparent on how to do, considering it is basically just replaying this
backwards.
```

- When you read the descriptions of the `LOAD` and `STR` instructions, you may
be thrown off by the fact that the little half of a register is stored first in memory.
This is actually on purpose. It is because this CPU stores bytes in little-endian format.
