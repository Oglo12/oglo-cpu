# Assembly Syntax Specification



### COMMENTS

This ASM flavor is just like all the other ASM flavors in that it
uses `;` to create comments.

Example:
```
; This is a very epic comment.
```

The assembler stops comments at new lines.



### REGISTERS

Any special registers are written with a `$` as the prefix.
For example, the instruction pointer register would be written like: `$ip`

For a list of special registers, look at: ***`Hardware-Specifications.md`***

All 16 general purpose registers of the CPU are written in the ASM like this:
- r0
- r1
- r2
- r3
- r4
- r5
- r6
- r7
- r8
- r9
- r10
- r11
- r12
- r13
- r14
- r15



### NUMBERS

To specify an integer, just type the integer.
To specify an integer in binary format, write the binary number like this: `0b<BITS>`
To specify an integer in hexadecimal format, write the hexadecimal number like this: `0x<HEX>`

When specifying a number as a memory address, prefix it with a `&`.

Examples: (`52`, `0b00101100`, `0xDF00AA`)



### WRITING INSTRUCTIONS

The syntax for writing instructions looks like this:
```
<INSTRUCTION> <ARGUMENTS>
```

Arguments are separated with commas. (Example: `INSTRUCTION ARG, ARG, ARG`)

Here is an example of some instructions:
```
LOAD r0, &0x00000000E00000DF
STR r1, &0x000000000000001F
ADD r0, r1, r2
COPY r2, r1
COPY r2, r0
LOAD r2, 0
```

To see a table of instructions and their arguments as well as what they do, look at: ***`ISA.md`***



### LABELS

The jump instructions use addresses in order to know where to go,
but you can also use labels as dynamic placeholders in the ASM.

Here is an example of how you define a label:
```
@MyLabel:
```

As you can see, you use a `@` symbol and then the name of your label followed by a colon.

When you want the assembler to evaluate a label argument as an address, you prefix it with `@`.
When you want the assembler to evaluate a label argument as an integer, you prefix it with `#`.

Here is an example of loading a label's value into a register, and also jumping to that label:
```asm
; '#' means as integer, and `@` means as address.

LOAD r0, #MyLabel ; Put the memory address for MyLabel as an integer into r0.

JUMP @MyLabel ; Jump to the address for MyLabel.

@MyLabel:
    HLT ; Stop the CPU.
```

When using the jump instructions, you specify the `@` symbol and then the name of the label.

Here is a full ASM example of creating a label, and then jumping to that label:
```asm
; This simple ASM program increments register 1 until it is equal to 15,
; and then stops the CPU.

LOAD r0, 1 ; Store the number to increment by. In this case, it is simply just 1.
LOAD r2, 15 ; Store the goal number. In this case, it is 15.

; Define the loop logic.
@MyLoop:
    ADD r1, r0, r1 ; Add the increment number to register 1.
    CMP r1, r2 ; Compare register 1 and register 2.
    JNZ @MyLoop ; If the `cmp` flag is not 0, jump to @MyLoop.

HLT ; Stop the CPU.
```

Infact, labels can be used anywhere, because they always refer to dynamic memory addresses!
(They get filled in with real memory addresses during the assembly phase.)

##### Quick Tip:

Labels can also be used as function definitions.

Here is an example of that:
```asm
; Instead of using jump instructions,
; this program uses 'CALL' and 'RET' to use labels as if they were functions.

; This program creates a function for calculating modulus.
; Then it uses that function for modulus to do modulus math on 2 numbers.
; Then it stops the CPU.

LOAD r0, 7 ; First number.
LOAD r1, 2 ; Second number.
LOAD r2, 0 ; The result register.

CALL @FnMod ; First Number % Second Number

COPY r15, r2 ; Copy the return value into the register we wanna put the result in.

JUMP @Exit

@FnMod:
    DIV r0, r1, r2 ; Divide r0 by r1, and store in r2.
    MUL r2, r1, r2 ; Scale division result by the divide-by number.
    SUB r0, r2, r0 ; Subtract r2 from r0, and store in r0.
    COPY r0, r15 ; Copy the value of r0 into r15. (This program's return convention I guess.)
    RET ; Return to where the 'CALL' instruction was invoked.

; Exit point. This is just to avoid running code under labels by accident.
@Exit:
    HLT ; Stop the CPU.
```



### CUSTOM INSTRUCTIONS

You can define custom instructions incase you are using a custom extended ISA or something.

Something to also keep in mind is that your custom instructions cannot exceed the maximum
current instruction size in bytes. For example, if the current biggest instruction is 11 bytes
in size, your custom instruction cannot exceed 11 bytes in size.

```asm
; This simple program defines custom instructions.
; QUICK NOTE: Defining an already existing instruction as a custom instruction will overwrite it.
; Another thing is that trying to use a custom instruction before it is defined will not work.

custom! CINS 0x9999 reg int mem ; This custom instruction has opcode 0x9999, and it expects a register, an integer, and a memory address.
custom! CINS 0x9991 reg mem ; You can also define variants.
custom! CINS 0x9992 ; You can also do instructions without arguments.

LOAD r0, <CINS reg int mem> ; Load the opcode of instruction CINS into register 0. (This works on normal instructions too.)
LOAD r1, <CINS reg mem> ; Load the opcode of instruction CINS (other variant) into register 1.
LOAD r2, <CINS> ; I think you understand now.

ASSERT r0, 0x9999 ; Just asserting that the opcode in register 0 is correct.
ASSERT r1, 0x9991 ; Just asserting that the opcode in register 1 is correct.
ASSERT r2, 0x9992 ; You get the point.

CINS r1, 0x4F, &0x0078F2 ; Call our new CINS instruction!
CINS r15, &0xC0FFEE ; Calling the other variant as well.
CINS ; Calling the variant that has no arguments.

; Now, let's see what happens with pre-existing instructions:

CMP r0, r1 ; Old CMP instruction.

custom! CMP 0x8888 reg reg ; Overwriting CMP (reg, reg). (The other CMP instruction variants are left alone.)

CMP r0, r1 ; New CMP instruction.
```



### MACROS

Macros are useful for defining many instructions as "one instruction."

```asm
macro! modulus %reg_1 %reg_2 %reg_3 {
    DIV %reg_1, %reg_2, %reg_3
    MUL %reg_3, %reg_2, %reg_3
    SUB %reg_1, %reg_3, %reg_3
}

LOAD r0, 12

LOAD r1, 5

modulus% r0, r1, r2

; Macros are basically glorified second files in a way, so you can put anything in them.
; (Second files as in the parser parses them using the file (root level) parsing rules.)
macro! my_really_strange_macro {
    custom! INS 0x0770 ; Custom instruction that only gets created when the macro gets called.
    INS ; Calling it.
}
```



### RAW DATA

Executables aren't only instructions, they are also data used with those instructions.

You can define things such as ASCII strings and raw bytes to be used in the executable.

```asm
; This simple little program puts the number 69 into memory address 2,
; and then it also puts the string "Hello, world!" into memory address 44.
; (Yes, I know that the string is being stored in big-endian.)

; Moving the integer 69 into memory address 2:
LOAD r0, @MyFunnyData
STR r0, &2

; Moving the string "Hello, world!" into memory address 44:
LOAD r1, #HelloWorldV2 ; You can use either V1 or V2. They do the same thing,
                       ; but with different syntax. :D
LOAD r2, 44 ; Destination address. (Used in the CTD instruction.)
CTD r1, r2 ; Copy all the data of the string to address 44. (Including NULL byte.)

; Just for showing off, here is an instruction that can be used to get the length
; of the string "Hello, world!" (excluding the NULL byte).
TLEN r8, @HelloWorldV2 ; This instruction counts how many bytes until a NULL byte,
                       ; and stores the result in register 8.
INC r8 ; Include the NULL byte in the final length.

HLT

@MyFunnyData:
.byte 69

@HelloWorldV1:
.ascii "Hello, world!"
.byte 0 ; NULL termination.

@HelloWorldV2:
.asciiz "Hello, world!" ; This is the same as '.ascii' but with NULL termination automatically.
```
