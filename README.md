# Oglo CPU

> Oglo CPU is now discontinued in favor of Oca64 (another better architecture I made).

Oglo CPU is a custom CPU architecture that I am working on.

You can check out things like the ASM syntax and the ISA in the `docs/` directory.
