#!/usr/bin/env bash

log_error () {
    echo -e "\033[0;31m[\033[0m \033[1;31mERROR\033[0m \033[0;31m]:\033[0m $1\033[0m" >&2
}

log_info () {
    echo -e "\033[0;32m[\033[0m \033[1;32mINFO\033[0m \033[0;32m]:\033[0m $1\033[0m"
}

log_debug () {
    if [[ "${DEBUG_LOG}" == "1" ]]; then
        echo -e "\033[0;35m[\033[0m \033[1;35mDEBUG\033[0m \033[0;35m]:\033[0m $1\033[0m"
    fi
}

die () {
    log_error "$1"
    exit 1
}

starting_dir="$(pwd)"

[[ -d ./docs ]] || die "Please run in project root!"

log_info "Building ISA.md..."
cd ./docs/isa_src || exit 1
./build.sh || exit 1
cd "${starting_dir}" || exit 1
